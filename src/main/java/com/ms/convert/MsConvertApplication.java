package com.ms.convert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsConvertApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsConvertApplication.class, args);
    }

}
