package com.ms.convert.init;

import com.ms.convert.utils.Utils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class SystemInit implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        Utils.loadLibrary(System.getProperty("user.dir"));
    }
}
