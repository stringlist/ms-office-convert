package com.ms.convert.controller;

import com.ms.convert.utils.LogUtils;
import com.ms.convert.utils.MsOfficeKit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 首页
 *
 * @author Dizzy
 */
@Slf4j
@RequestMapping
@RestController
@RequiredArgsConstructor
public class IndexController {


    @RequestMapping
    public String index() {
//        MsOfficeKit.ppt2PDF("E:/msconvert/ppt.pptx", "E:/msconvert/ppt.pdf");
//        MsOfficeKit.word2PDF("E:/msconvert/doc.docx", "E:/msconvert/doc.pdf");

//        MsOfficeKit.ppt2PDF("E:/msconvert/ppt.pptx", "E:\\msconvert\\ppt.pdf");

        LogUtils utils = LogUtils.startRecord();
        MsOfficeKit msOfficeKit = new MsOfficeKit();
        msOfficeKit.convert2PDF("E:\\msconvert\\ppt.pptx");
        utils.stopRecord("转换PPT时长");
        return "(｡･∀･)ﾉﾞ";
    }


}
